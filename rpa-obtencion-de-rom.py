"""
    RPA para automatizacion de captura de datos desde un XML y una pagina web a un archivo de Excel
    usando Python3, Selenium

    Autor:
        Eng. Stephanie Poleo Ruiz
"""
############################################################################
#                       Imports Básicos                                    #
############################################################################

import pandas as pd 
import xml.etree.ElementTree as et 
import ast
import math
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import StaleElementReferenceException
from datetime import datetime
from difflib import SequenceMatcher
import time
import utils
import logging
import sys, os

#######################################################
#                Variables Globales                   #
#######################################################

df_cols = ['Placa', 'Rom', 'Cliente', 'F. Certificacion', 'F. Ultima Revision', 'F. Proxima Revision']
rows = []


#Pagina de adquision de datos
pagina = 'http://natgas.gasdata.com.co/Login.aspx'
user = 'ADMINP'
password = '8462'


###############################################################################
#                                 Funciones                                   #
###############################################################################

#Funciones que llevan a cabo la instalacion del driver necesario para Selenium
def install(cwd=False):
    """
    Appends the directory of the chromedriver binary file to PATH.
    :param cwd: Flag indicating whether to download to current working directory
    :return: The file path of chromedriver
    """
    chromedriver_filepath = utils.download_chromedriver(cwd)
    if not chromedriver_filepath:
        logging.debug('Can not download chromedriver.')
        return
    chromedriver_dir = os.path.dirname(chromedriver_filepath)
    if 'PATH' not in os.environ:
        os.environ['PATH'] = chromedriver_dir
    elif chromedriver_dir not in os.environ['PATH']:
        os.environ['PATH'] = chromedriver_dir + utils.get_variable_separator() + os.environ['PATH']
    return chromedriver_filepath

def get_chrome_version():
    """
    Get installed version of chrome on client
    :return: The version of chrome
    """
    return utils.get_chrome_version()

def info_pagina_web(driver, placa):
    # Inicializacion del navegador para abrir la pagina web
    
    try: 
        info = []
        print("Empezando analisis")

        driver.find_element_by_id("ContentPlaceHolder1_ctrl_btnLimpiar").click()
        time.sleep(0.5)

        driver.find_element_by_id("ContentPlaceHolder1_ctrl_txtBuscarPlaca").send_keys(placa)
        driver.find_element_by_id("ContentPlaceHolder1_ctrl_btnBuscarSuic").click()

        extra = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr/td').text   

        info.append(placa)

        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[2]'))
        )

        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[2]'))
        )

        rom = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[2]').text
        info.append(rom)

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[3]/span'))
        )
        cliente = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[3]/span').text
        info.append(cliente)

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[4]'))
        )
        f_certificacion = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[4]').text
        info.append(f_certificacion)

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[5]'))
        )
        f_ult_rev = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[5]').text
        info.append(f_ult_rev)

        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located((By.XPATH, '/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[6]'))
        )
        f_prox_rev = driver.find_element_by_xpath('/html/body/form/div[3]/div/div[2]/div[2]/div[3]/div[3]/div[1]/div/div/div/div[5]/div/div/table/tbody/tr[2]/td[6]').text
        info.append(f_prox_rev)  

        print("Click boton")
        driver.find_element_by_id("ContentPlaceHolder1_ctrl_btnLimpiar").click()

    except TimeoutException as e:

        print("TimeoutException has been thrown. " + str(e))
        info.append(extra)
        info.append("")
        info.append("")
        info.append("")
        info.append("")

    except NoSuchElementException as e:
        print("NoSuchElementException has been thrown. " + str(e))
        crear_dataFrame(info)

    except WebDriverException as e: 
        message = str(e.args[0]).split('\n')
        print(message)
        if(message[0] == "chrome not reachable"):
            print("El navegador fue cerrado")
            out_df = pd.DataFrame(rows, columns=df_cols)
            crear_excel(out_df, driver)
            k = input("Presione ENTER para terminar el RPA.")
            sys.exit(1)
        elif(message[0] == "no such window:"):
            print("El navegador fue cerrado")
            out_df = pd.DataFrame(rows, columns=df_cols)
            crear_excel(out_df, driver)
            k = input("Presione ENTER para terminar el RPA.")
            sys.exit(1)
        else:
            print("WebDriverException has been thrown. " + str(e))
            info_pagina_web(driver, placa)

    except AttributeError as e:
        print("AttributeError has been thrown. " + str(e))
        crear_dataFrame(info)

    except ElementClickInterceptedException as e: 
        print("ElementClickInterceptedException has been thrown. " + str(e))
        info_pagina_web(driver, placa)

    except StaleElementReferenceException as e:
        print("StaleElementReferenceException has been thrown. " + str(e))
        info_pagina_web(driver, placa)

    except RecursionError as e:
        print("RecursionError has been thrown. " + str(e))

        print("Cerrando todo")
        driver.delete_all_cookies()

        print("Espera")
        time.sleep(4)

        print("Volviendo a empezar")
        driver.maximize_window()
        driver.get(pagina)
        driver.find_element_by_id("ContentPlaceHolder1_txtUserName").send_keys(user)
        driver.find_element_by_id("ContentPlaceHolder1_txtPassword").send_keys(password)
        driver.find_element_by_id("ContentPlaceHolder1_btnLogin").click()

        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="ui-accordion-accordionMaster-panel-0"]/ul/li[2]/a'))
        ).click()

        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, 'Button8'))
        ).click()

        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, 'Button5'))
        ).click()
        time.sleep(3)
        info_pagina_web(driver, placa)

    crear_dataFrame(info, driver)
    
def lecturaArchivoPlacas(driver):

    # Working directories.
    if getattr(sys, 'frozen', False):
        path = os.path.dirname(sys.executable)
    else:
        path = os.path.dirname(os.path.abspath(__file__))

    listing = os.listdir(path)
    archivos = []
    primera = False

    for infile in listing:

        if(infile.startswith("PlacasLeon")):
            archivos.append(infile)

    try: 
        archivos.sort(reverse = True)
        print(archivos)

        print(archivos[0])

        ult_placa = pd.read_excel(archivos[0])
        ult_placa = ult_placa['Unnamed: 0'].iloc[-1]

        print(ult_placa)
    except IndexError:
        print("Ejecutando RPA por primera vez")
        primera = True

    try:
        catalogo = pd.read_excel("placas León.xlsx")
        for index, row in catalogo.iterrows():
        # Guardar taller
            placa = str(row).splitlines()[0].split('Placa')
            placa = placa[1].strip()

            if(primera == True):
                print("Buscando placa: " + str(placa))
                info_pagina_web(driver,placa) 
                print("Se ha registrado la información de " + str(index + 1) + " placa(s).")

            elif(primera == False):
                if(placa==ult_placa):
                    primera = True
    
    except Exception as e:
        print("Hubo un error abriendo el archivo de placas: " + str(e))

    out_df = pd.DataFrame(rows, columns=df_cols)
    crear_excel(out_df, driver)

# Crear el data frame con la informacion organizada
def crear_dataFrame(info, driver):

    print(info)
    
    try:
        rows.append({df_cols[i]: info[i] for i, _ in enumerate(df_cols)})
    
    except IndexError as e:
        print("IndexError has been thrown. " + str(e))
    
# Crea el excel que almacenara el dataframe final
def crear_excel(df, driver):
    # Crea un writer en Pandas Excel para poder usar el XlsxWriter
    #print(datetime.now().strftime('%d%m%Y%H%M')) #formato 24
    try:
        writer = pd.ExcelWriter('PlacasLeon_' + datetime.now().strftime('%d%m%Y%H%M') + '.xlsx', engine='xlsxwriter')
        # Convierte el dataframe en un objeto de Excel XlsxWriter
        df.to_excel(writer, sheet_name='Datos', index=False, startrow=1, header=False)

        # Cierra el writer de Pandas Excel y guarda la informacion en un archivo de Excel 
        writer.save()

    except PermissionError:
        print("El archivo final no pudo ser creado/modificado porque se encuentra abierto, cierrelo y vuelva a ejecutar el programa")
        driver.quit()
        k = input("Presione ENTER para terminar el RPA.")
        sys.exit(1)

def runRpa():

    #Configuracion de las opciones de navegacion con Selenium
    install()
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    driver = webdriver.Chrome(options=options)
    driver.maximize_window()
    driver.get(pagina)

    #Inicio de sesion
    driver.find_element_by_id("ContentPlaceHolder1_txtUserName").send_keys(user)
    driver.find_element_by_id("ContentPlaceHolder1_txtPassword").send_keys(password)
    driver.find_element_by_id("ContentPlaceHolder1_btnLogin").click()

    WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, '//*[@id="ui-accordion-accordionMaster-panel-0"]/ul/li[2]/a'))
    ).click()

    WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.ID, 'Button8'))
    ).click()

    WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.ID, 'Button5'))
    ).click()

    lecturaArchivoPlacas(driver)

    driver.quit()

###############################################################################
#                                   Main                                      #
###############################################################################

if __name__ == '__main__':

    start_time = time.time()
    runRpa()
    print("\nExecution Time: %s" %(time.time() - start_time))
    #Enter para terminar
    print("Terminó exitosamente!!")
    x=input("PRESIONA EXNTER PARA TERMINAR")
